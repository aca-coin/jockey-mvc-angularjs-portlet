package be.aca.devcon.jockey.mvc.internal.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import org.osgi.service.component.annotations.Component;

import javax.portlet.Portlet;

@Component(
	immediate = true,
	service = Portlet.class,
	property = {
		"com.liferay.portlet.display-category=ACA Devcon",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.header-portal-javascript=/o/jockey-mvc-angularjs-portlet/js/angular.js",
		"com.liferay.portlet.footer-portlet-javascript=/js/service.js",
		"com.liferay.portlet.footer-portlet-javascript=/js/main.js",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.display-name=Jockey MVC AngularJS portlet",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.name=JockeyMVCAngularJSPortlet"
	}
)
public class JockeyMVCAngularJSPortlet extends MVCPortlet {

}




