package be.aca.devcon.jockey.mvc.internal.command;

import be.aca.devcon.jockey.service.api.Jockey;
import be.aca.devcon.jockey.service.api.JockeyService;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

@Component(
		immediate = true,
		property = {
				"javax.portlet.name=JockeyMVCPrimeUIPortlet",
				"mvc.command.name=/jockey/detail"
		},
		service = MVCRenderCommand.class
)
public class JockeyDetailRenderCommand implements MVCRenderCommand {

	@Reference private JockeyService jockeyService;

	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		int id = ParamUtil.getInteger(renderRequest, "id");

		Jockey jockey = jockeyService.getJockey(id);

		renderRequest.setAttribute("jockey", jockey);

		return "/detail.jsp";
	}
}
