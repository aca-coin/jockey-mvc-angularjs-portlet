package be.aca.devcon.jockey.mvc.internal.command;

import be.aca.devcon.jockey.service.api.JockeyService;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONSerializer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.StringPool;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@Component(
		immediate = true,
		property = {
				"javax.portlet.name=JockeyMVCPrimeUIPortlet",
				"mvc.command.name=/jockey/get-teams"
		},
		service = MVCResourceCommand.class
)
public class GetTeamsResourceCommand implements MVCResourceCommand {

	private static final Log LOGGER = LogFactoryUtil.getLog(GetTeamsResourceCommand.class);

	@Reference private JockeyService jockeyService;
	@Reference private JSONFactory jsonFactory;

	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException {
		JSONSerializer serializer = jsonFactory.createJSONSerializer();

		List<String> teams = new ArrayList<>();
		teams.add(StringPool.BLANK);
		teams.addAll(jockeyService.getTeams());

		try {
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(serializer.serialize(teams));

			return false;
		} catch (IOException e) {
			LOGGER.error(e);

			return true;
		}
	}
}
