package be.aca.devcon.jockey.mvc.internal.command;

import be.aca.devcon.jockey.service.api.JockeyService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component(
		immediate = true,
		property = {
				"javax.portlet.name=JockeyMVCPrimeUIPortlet",
				"mvc.command.name=/jockey/count"
		},
		service = MVCResourceCommand.class
)
public class CountJockeysResourceCommand implements MVCResourceCommand {

	private static final Log LOGGER = LogFactoryUtil.getLog(GetTeamsResourceCommand.class);

	@Reference private JockeyService jockeyService;

	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException {
		String team = ParamUtil.getString(resourceRequest, "team");
		team = team.isEmpty() ? null : team;

		int count = jockeyService.getJockeysCount(team);

		try {
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(String.valueOf(count));

			return false;
		} catch (IOException e) {
			LOGGER.error(e);

			return true;
		}
	}
}
