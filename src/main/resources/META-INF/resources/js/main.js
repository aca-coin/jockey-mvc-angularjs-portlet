'use strict';

function bootstrapRest(id, portletId) {

	var app = angular.module(id, ["rest.factories"]);

	app.config(['urlProvider',
		function(urlProvider) {
			urlProvider.setPid(portletId);
		}
	]);

	app.controller("ListCtrl", ['$scope', 'url', 'jockeyFactory',
		function($scope, url, jockeyFactory) {
			$scope.portletId = portletId.substr(1, portletId.length - 2);

			$scope.page = url.createRenderUrl('jockeys');

			$scope.model = {};

			jockeyFactory.getJockeys().then(function(jockeys) {
				$scope.model.jockeys = jockeys;
			});
		}]
	);

	angular.bootstrap(document.getElementById(id),[id]);
}