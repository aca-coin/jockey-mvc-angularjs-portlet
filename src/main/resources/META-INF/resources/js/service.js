'use strict';

angular.module("rest.factories", []).

provider('url', function() {
	this.pid = '';

	this.$get = function() {
		var pid = this.pid;
		return {
			createRenderUrl: function(page) {
				var resourceURL = Liferay.PortletURL.createRenderURL();
				resourceURL.setPortletId(pid);
				resourceURL.setPortletMode('view');
				resourceURL.setWindowState('exclusive');
				resourceURL.setParameter('jspPage', '/partials/' + page + '.jsp');

				console.log("URL " + resourceURL.toString());

				return resourceURL.toString();
			},
			createResourceUrl: function(resourceId) {
				// Need to set both resourceId and portletId for request to work
				// resourceId can be used to check and distinguish on server side
				var resourceURL = Liferay.PortletURL.createResourceURL();
				resourceURL.setPortletId(pid);
				resourceURL.setResourceId(resourceId);

				return resourceURL.toString();
			}
		}
	};

	this.setPid = function(pid) {
		this.pid = pid.substr(1, pid.length - 2);
	};
}).
factory('jockeyFactory', ['$q', '$http', 'url', function($q, $http, url) {
	var getJockeys = function() {
		var deferred = $q.defer();
		var resource = url.createResourceUrl("/jockey/search");

		$http.get(resource.toString()).success(function(data, status, headers, config) {
			deferred.resolve(data);
		});

		return deferred.promise;
	};

	return {
		getJockeys: getJockeys
	};
}]);