<table class="table table-striped table-hover table-responsive">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Horse</th>
			<th>Team</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr ng-repeat="jockey in model.jockeys track by $index">
			<td>{{jockey.id}}</td>
			<td>{{jockey.name}}</td>
			<td>{{jockey.horse}}</td>
			<td>{{jockey.teamName}}</td>
			<td><a href="#">View</a></td>
		</tr>
	</tbody>
</table>